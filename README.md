## Module 5: Elastic Compute Services

**Due to the unavailability of the discussed microservices application, this task will be completed using a simple NGINX application containing only a basic homepage**

### Sub-task 1 – Store Docker image to S3

**1. Create a Dockerfile with my application**

![1.1](screenshots/1.1.png)

**2. Create image and save it as .tar**

![1.2](screenshots/1.2.png)

**3. Upload image to the S3 Bucket from previous module**

![1.3](screenshots/1.3.png)


### Sub-task 2 - Create EC2 instance

**1. Create Linux EC2 instance for web application**

![2.1](screenshots/2.1.png)

**2. Assign it the S3 readonly IAM role from module 2**

![2.2](screenshots/2.2.png)

**3. Configure security group for the EC2 instance to allow HTTP and SSH**

![2.3](screenshots/2.3.png)

**4. Make sure nginx server is installed and running on the instance and it starts whenever the instance boot/reboot.**

![2.4.1.](screenshots/2.4.1.png)

![2.4.2.](screenshots/2.4.2.png)

![2.4.2.](screenshots/2.4.3.png)

### Sub-task 3 - Run Docker container

**1. Connect to the EC2 instance via SSH**

![3.1](screenshots/3.1.png)

**2. Download .tar docker image from S3 bucket**

![3.2](screenshots/3.2.png)

**3. Install and start docker**

![3.3](screenshots/3.3.png)

**4. Load docker image from .tar file**

![3.4](screenshots/3.4.png)

**5. Run docker container**

![3.5](screenshots/3.5.png)

**6. Make sure the home page is available outside EC2**

![3.6](screenshots/3.6.png)

### Sub-task 4 – automate EC2 configuration

**1. Create a new application EC2 instance**

![4.1.1](screenshots/4.1.1.png)

**2. Configure the new EC2 instance so that it will automatically run docker container**

![4.2](screenshots/4.2.png)

**3. Ensure that home page can be accessed**

![4.3](screenshots/4.3.png)

**4. Create a custom AMI based on the EC2 instance**

![4.4](screenshots/4.4.png)

**5. Delete the EC2 instance and create another one based on the custom AMI.**

![4.5.1](screenshots/4.5.1.png)

![4.5.2](screenshots/4.5.2.png)

**6. Make sure the home page is still available over HTTP**

![4.6](screenshots/4.6.png)

### Sub-task 5 - Launch Report microservice instance

**I skipped this step cause my application does not have any integration**

### Sub-task 6 – Introducing EBS basics

**1. Create EBS volume and attach it to the EC2 instance**

![6.1.1](screenshots/6.1.1.png)

![6.1.2](screenshots/6.1.2.png)

**2. Write any file to it and detach from the instance**

![6.2.1](screenshots/6.2.1.png)

![6.2.2](screenshots/6.2.2.png)

![6.2.3](screenshots/6.2.3.png)

**3. Attach it to the instance and make sure the file is visible and accessible**

![6.3.1](screenshots/6.3.1.png)

![6.3.2](screenshots/6.3.2.png)
